package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
)

type CollectionTable struct {
	gorm.Model
	Name     string
	Seasons  string
	Style    string
	Gender   string
	Location string
	UserID   uint
}

func (CollectionTable) TableName() string {
	return "collections"
}

type ItemTable struct {
	gorm.Model
	CollectionID uint
	Name         string
	Image        string
}

func (ItemTable) TableName() string {
	return "items"
}

type CollectionService interface {
	CreateCollection(collection *CollectionTable) error
	CreateItem(item *ItemTable) error

	GetRandomCollection() (*CollectionTable, error)
	GetOneItemByCollectionID(collection_id uint) (*ItemTable, error)
	GetItemByCollectionID(collection_id uint, search string) ([]ItemTable, error)
	GetCollectionByFilter(gender string, style string, location string, seasons string, search string) ([]CollectionTable, error)

	GetCollectionByID(id uint) ([]ItemTable, error)
	GetCollectionByUserID(id uint) ([]Collection, error)
}

type CollectionGorm struct {
	db *gorm.DB
}

func NewCollectionGorm(db *gorm.DB) CollectionService {
	return &CollectionGorm{db}
}

func (cs *CollectionGorm) CreateCollection(collection *CollectionTable) error {
	return cs.db.Create(collection).Error
}

func (cs *CollectionGorm) CreateItem(item *ItemTable) error {
	return cs.db.Create(item).Error
}

func (cs *CollectionGorm) GetRandomCollection() (*CollectionTable, error) {
	collection := CollectionTable{}
	err := cs.db.Order("RAND()").Limit(1).Find(&collection).Error
	if err != nil {
		return nil, err
	}
	return &collection, nil
}

func (cs *CollectionGorm) GetOneItemByCollectionID(collection_id uint) (*ItemTable, error) {
	itemTable := ItemTable{}
	err := cs.db.Where("collection_id = ?", collection_id).Limit(1).Find(&itemTable).Error
	if err != nil {
		return nil, err
	}
	return &itemTable, nil
}

func (cs *CollectionGorm) GetItemByCollectionID(collection_id uint, search string) ([]ItemTable, error) {
	itemTable := []ItemTable{}
	err := cs.db.Where("collection_id = ? AND name LIKE ?", collection_id, "%"+search+"%").Limit(10).Find(&itemTable).Error
	if err != nil {
		return nil, err
	}
	return itemTable, nil
}

func (cs *CollectionGorm) GetCollectionByFilter(gender string, style string, location string, seasons string, search string) ([]CollectionTable, error) {
	collection := []CollectionTable{}
	err := cs.db.Where("style LIKE ? AND gender LIKE ? AND location LIKE ? AND seasons LIKE ? AND name LIKE ?", style, gender, location, seasons, search).Limit(10).Find(&collection).Error
	if err != nil {
		return nil, err
	}
	return collection, nil
}

func (cs *CollectionGorm) GetCollectionByID(id uint) ([]ItemTable, error) {
	items := []ItemTable{}
	err := cs.db.Where("collection_id = ?", id).Find(&items).Error
	if err != nil {
		return nil, err
	}
	return items, nil
}

type Collection struct {
	ID       uint
	Name     string
	Seasons  string
	Style    string
	Gender   string
	Location string
	UserID   uint
}

func (cs *CollectionGorm) GetCollectionByUserID(id uint) ([]Collection, error) {
	collection := []Collection{}
	rows, err := cs.db.Table("users").
		Select("collections.id , collections.name ,collections.seasons ,collections.style,collections.gender,collections.location,collections.user_id").
		Joins("join collections ON users.id = collections.user_id").
		Where("users.id = ?", id).Rows()
	if err != nil {
		fmt.Println(err)
	}
	for rows.Next() {
		report := Collection{}
		err := rows.Scan(&report.ID, &report.Name, &report.Seasons, &report.Style, &report.Gender, &report.Location, &report.UserID)
		if err != nil {
			fmt.Println(err)
		}
		collection = append(collection, report)
	}
	if err != nil {
		return nil, err
	}
	return collection, nil

}

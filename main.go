package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"log"
	"stylish/config"
	"stylish/handlers"
	"stylish/models"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {

	conf := config.Load()

	db, err := gorm.Open("mysql", conf.Connection)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	if conf.Mode == "dev" {
		db.LogMode(true) // dev only!
	}

	if err := db.AutoMigrate(
		&models.CollectionTable{},
		&models.ItemTable{},
		&models.UserTable{},
	).Error; err != nil {
		log.Fatal(err)
	}

	cs := models.NewCollectionGorm(db) //collection

	ch := handlers.NewCollectionHandler(cs) //collection

	mac := hmac.New(sha256.New, []byte(conf.HMACKey))

	us := models.NewUserGorm(db, mac) //user

	uh := handlers.NewUserHandler(us, mac) //user

	if conf.Mode != "dev" {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"*"}
	config.AllowHeaders = []string{"*"}

	r.Use(cors.New(config))

	r.Static("/images", "./upload")

	r.GET("/randomCollection", ch.GetRandomCollection)

	r.POST("/login", uh.Login)

	r.POST("/signup", uh.Signup)

	authorized := r.Group("/")
	authorized.Use(uh.Authorization)
	{
		authorized.GET("/user", uh.GetUserData)
		authorized.GET("/collection/:id", ch.GetCollectionByID)
		authorized.GET("/collectionByUser", ch.GetCollectionByUserID)
		authorized.POST("/collectionByFilter", ch.GetCollectionByFilter)
		authorized.POST("/itemsByFilter", ch.GetItemsByFilter)
		authorized.POST("/createCollection", ch.CreateCollection)
		authorized.POST("/createItem/:id", ch.CreateItem)
		authorized.PATCH("/username", uh.UpdateUsername)
		authorized.PATCH("/logout", uh.Logout)
		authorized.PATCH("/resetPassword", uh.ResetPassword)

	}

	r.Run(":8080")
}

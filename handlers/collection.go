package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"stylish/models"

	"github.com/gin-gonic/gin"
)

type Collection struct {
	CollectionID uint
	Name         string
	Seasons      string
	Style        string
	Gender       string
	Location     string
	UserID       uint
}

type Item struct {
	ItemID       uint
	CollectionID uint
	Name         string
	Image        string
}

type CollectionHandler struct {
	cs models.CollectionService
}

func NewCollectionHandler(cs models.CollectionService) *CollectionHandler {
	return &CollectionHandler{cs}
}

// CREATE
type reqAddCollection struct {
	Name     string
	Seasons  string
	Style    string
	Gender   string
	Location string
	UserID   string
}

func (ch *CollectionHandler) CreateCollection(c *gin.Context) {

	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	id := user.ID

	collection := new(reqAddCollection)
	if err := c.BindJSON(collection); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}
	Collection := new(models.CollectionTable)
	Collection.Name = collection.Name
	Collection.Seasons = collection.Seasons
	Collection.Style = collection.Style
	Collection.Gender = collection.Gender
	Collection.Location = collection.Location
	Collection.UserID = id

	if err := ch.cs.CreateCollection(Collection); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	c.JSON(200, gin.H{
		"collection_id": Collection.ID,
	})
}

type reqItem struct {
	Name  string
	Image string
}

func (ch *CollectionHandler) CreateItem(c *gin.Context) {

	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)

	if err != nil {
		fmt.Println(err)
	}
	collectionID := uint(u64)

	item := new(reqItem)
	if err := c.BindJSON(item); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}
	itemsTable := new(models.ItemTable)
	itemsTable.CollectionID = collectionID
	itemsTable.Name = item.Name
	itemsTable.Image = item.Image
	if err := ch.cs.CreateItem(itemsTable); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
	}
	c.Status(200)
}

//GET
func (ch *CollectionHandler) GetRandomCollection(c *gin.Context) {

	collection, err := ch.cs.GetRandomCollection()
	if err != nil {
		c.Status(500)
		return
	}
	search := "%"
	tts, err := ch.cs.GetItemByCollectionID(collection.ID, search)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	items := []Item{}
	for _, tt := range tts {
		items = append(items, Item{
			ItemID:       tt.ID,
			CollectionID: tt.CollectionID,
			Name:         tt.Name,
			Image:        tt.Image,
		})
	}
	c.JSON(http.StatusOK, items)
}

type reqFilter struct {
	Gender   string
	Location string
	Style    string
	Seasons  string
}

func (ch *CollectionHandler) GetCollectionByFilter(c *gin.Context) {

	Search := c.Query("Search")
	if Search == "" {
		Search = "%"
	}
	if Search != "" {
		Search = "%" + Search + "%"
	}

	fmt.Println(Search)
	filter := new(reqFilter)
	if err := c.BindJSON(filter); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}

	gender := filter.Gender
	style := filter.Style
	location := filter.Location
	seasons := filter.Seasons

	collection, err := ch.cs.GetCollectionByFilter(gender, style, location, seasons, Search)
	if err != nil {
		c.Status(500)
		return
	}
	col := []models.ItemTable{}
	for _, tt := range collection {
		oneItem, err := ch.cs.GetOneItemByCollectionID(tt.ID)
		if err != nil {
			c.Status(500)
			return
		}
		col = append(col, models.ItemTable{
			CollectionID: oneItem.CollectionID,
			Name:         oneItem.Name,
			Image:        oneItem.Image,
		})
	}
	c.JSON(200, col)
}

func (ch *CollectionHandler) GetItemsByFilter(c *gin.Context) {

	Search := "%"
	filter := new(reqFilter)
	if err := c.BindJSON(filter); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}
	gender := filter.Gender
	style := filter.Style
	location := filter.Location
	seasons := filter.Seasons
	collection, err := ch.cs.GetCollectionByFilter(gender, style, location, seasons, Search)
	if err != nil {
		c.Status(500)
		return
	}

	search := c.Query("search")
	if search == "" {
		search = "%"
	}
	if search != "" {
		search = "%" + search + "%"
	}
	items := []Item{}
	for _, col := range collection {
		tts, err := ch.cs.GetItemByCollectionID(col.ID, search)
		if err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}

		for _, tt := range tts {
			items = append(items, Item{
				ItemID:       tt.ID,
				CollectionID: tt.CollectionID,
				Name:         tt.Name,
				Image:        tt.Image,
			})
		}
	}
	c.JSON(http.StatusOK, items)
}

func (ch *CollectionHandler) GetCollectionByID(c *gin.Context) {

	id := c.Param("id")
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		fmt.Println(err)
	}
	gallery_id := uint(u64)

	tts, err := ch.cs.GetCollectionByID(gallery_id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	items := []Item{}
	for _, tt := range tts {
		items = append(items, Item{
			ItemID:       tt.ID,
			CollectionID: tt.CollectionID,
			Name:         tt.Name,
			Image:        tt.Image,
		})
	}
	c.JSON(http.StatusOK, items)
}

func (ch *CollectionHandler) GetCollectionByUserID(c *gin.Context) {

	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	user_id := user.ID

	collection, err := ch.cs.GetCollectionByUserID(user_id)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, collection)
}

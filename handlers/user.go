package handlers

import (
	"encoding/base64"
	"fmt"
	"hash"
	"net/http"
	"stylish/models"
	"stylish/rand"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

type Signup struct {
	Email string
}

type User struct {
	ID    uint
	Email string
}

type UserHandler struct {
	us   models.UserService
	hmac hash.Hash
}

func NewUserHandler(us models.UserService, hmac hash.Hash) *UserHandler {
	return &UserHandler{us, hmac}
}

type reqSignup struct {
	Username string
	Email    string
	Password string
}

func (uh *UserHandler) Signup(c *gin.Context) {

	signup := new(reqSignup)
	if err := c.BindJSON(signup); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}

	Username := signup.Username
	Email := signup.Email
	Password := signup.Password

	Signup := new(models.UserTable)
	Signup.Username = Username
	Signup.Email = Email
	hash, err := bcrypt.GenerateFromPassword([]byte(Password), 12)
	if err != nil {
		c.Status(500)
		return
	}
	Signup.Password = string(hash)

	token, err := rand.GetToken()

	uh.hmac.Write([]byte(token))
	expectedMAC := uh.hmac.Sum(nil)
	uh.hmac.Reset()
	Signup.Token = base64.URLEncoding.EncodeToString(expectedMAC)

	err = uh.us.Signup(Signup)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"Email": Signup.Email,
		"ID":    Signup.ID,
		"token": token,
	})
}

func (uh *UserHandler) GetUserData(c *gin.Context) {

	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	c.JSON(200, user)
}

type reqUsername struct {
	Username string
}

func (uh *UserHandler) UpdateUsername(c *gin.Context) {

	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	user_id := user.ID

	req := new(reqUsername)
	if err := c.BindJSON(req); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}
	username := req.Username
	err := uh.us.UpdateUsername(user_id, username)
	if err != nil {
		c.JSON((401), gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"id":       user_id,
		"username": username,
	})
}

type reqLogin struct {
	Email    string
	Password string
}

func (uh *UserHandler) Login(c *gin.Context) {

	login := new(reqLogin)
	if err := c.BindJSON(login); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}
	user := new(models.Login)
	user.Email = login.Email
	user.Password = login.Password

	token, err := uh.us.Login(user)
	if err != nil {
		c.JSON((401), gin.H{
			"message": err.Error(),
		})
		return
	}
	userData, err := uh.us.GetUserByToken(token)
	if err != nil {
		c.JSON((401), gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"token": token,
		"ID":    userData.ID,
	})
}

func (uh *UserHandler) Authorization(c *gin.Context) {
	header := c.GetHeader("Authorization")
	token := header[7:]

	user, err := uh.us.GetUserByToken(token)
	if err != nil {
		c.Status(401)
		c.Abort()
		return
	}
	c.Set("user", user)
}

func (uh *UserHandler) Logout(c *gin.Context) {

	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}

	fmt.Println(user.ID)

	err := uh.us.Logout(user.ID)
	if err != nil {
		fmt.Println("50000")
		c.Status(500)
		return
	}
	fmt.Println("20444444")
	c.Status(204)
}

type reqResetPassword struct {
	Password string
}

func (uh *UserHandler) ResetPassword(c *gin.Context) {

	user, ok := c.Value("user").(*models.UserTable)
	if !ok {
		c.JSON(401, gin.H{
			"message": "invalid token",
		})
		return
	}
	user_id := user.ID

	pw := new(reqResetPassword)
	if err := c.BindJSON(pw); err != nil {
		c.JSON((400), gin.H{
			"message": err.Error(),
		})
		return
	}

	Password := pw.Password

	hash, err := bcrypt.GenerateFromPassword([]byte(Password), 12)
	if err != nil {
		c.Status(500)
		return
	}

	err = uh.us.ResetPassword(user_id, string(hash))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(200, gin.H{
		"message": "reset password success",
	})
}
